package com.kkx.player;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiManager.WifiLock;
import android.os.IBinder;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;

import com.kkx.player.YouTubeExtractor.YouTubeExtractorListener;
import com.kkx.player.YouTubeExtractor.YouTubeExtractorResult;

import java.io.IOException;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author Maxim Ambroskin kkxmshu@gmail.com
 * @since 22/12/16
 */
public class PlayerService extends Service
        implements OnPreparedListener, OnCompletionListener, OnErrorListener {

    public static final String EXTRA_LINK = "EXTRA_LINK";

    public interface Action {
        String BASE = "com.kkx.player.ACTION_";

        String YOUTUBE = BASE + "YOUTUBE";
        String PLAY_PAUSE = BASE + "PLAY_PAUSE";
        String STOP = BASE + "STOP";
    }

    @IntDef({State.PAUSE, State.PLAYING})
    @Retention(RetentionPolicy.RUNTIME)
    public @interface State {
        int PAUSE = 0xAA;
        int PLAYING = 0xBB;
    }

    private static final int NOTIFICATION_ID = 777;

    private MediaPlayer mPlayer;

    private WifiLock mWifiLock;
    private NotificationManager mNotificationManager;
    private Notification.Builder mBuilder;

    @State
    private int mState;

    @Override
    public void onCreate() {
        super.onCreate();
        mWifiLock = ((WifiManager) getSystemService(Context.WIFI_SERVICE))
                .createWifiLock(WifiManager.WIFI_MODE_FULL, getClass().getSimpleName());
        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        final String action = intent.getAction();
        switch (action) {
            case Action.YOUTUBE:
                final String videoId = Utils.parse(intent.getStringExtra(EXTRA_LINK));
                final YouTubeExtractor extractor = new YouTubeExtractor(videoId);
                extractor.startExtracting(mExtractorListener);
                break;

            case Action.PLAY_PAUSE:
                if (mState == State.PLAYING) {
                    mPlayer.pause();
                    mState = State.PAUSE;
                } else {
                    mPlayer.start();
                    mState = State.PLAYING;
                }
                break;

            case Action.STOP:
                release(true);
                break;
        }

        return START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void release(boolean releaseMediaPlayer) {
        stopForeground(true);

        if (releaseMediaPlayer && mPlayer != null) {
            mPlayer.reset();
            mPlayer.release();
            mPlayer = null;
        }

        // TODO: 22/12/16 add in future
        if (mWifiLock.isHeld()) {
            mWifiLock.release();
        }
    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        mState = State.PLAYING;
        mPlayer.start();
        setUpAsForeground(getString(R.string.playing));
    }

    @SuppressWarnings("deprecation")
    private void setUpAsForeground(String text) {
        final PendingIntent pendingIntent = PendingIntent.getService(this, 0,
                new Intent(Action.STOP),
                PendingIntent.FLAG_UPDATE_CURRENT);

        final PendingIntent pendingIntent1 = PendingIntent.getService(this, 0, new Intent(Action.PLAY_PAUSE)
                , PendingIntent.FLAG_UPDATE_CURRENT);

        mBuilder = new Notification.Builder(getApplicationContext())
                .setSmallIcon(R.drawable.vec_android)
                .setContentTitle(getString(R.string.app_name))
                .addAction(0, getString(R.string.pause), pendingIntent1)
                .setContentText(text)
                .setContentIntent(pendingIntent)
                .setOngoing(true);

        startForeground(NOTIFICATION_ID, mBuilder.build());
    }


    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
        release(true);
    }

    @Override
    public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
        return false;
    }

    private final YouTubeExtractorListener mExtractorListener = new YouTubeExtractorListener() {
        @Override
        public void onSuccess(YouTubeExtractorResult result) {
            Utils.toLog(result.toString());
            Utils.toLog(result.getVideoUri().toString());
            if (mPlayer == null || !mPlayer.isPlaying()) {
                mPlayer = new MediaPlayer();

                try {
                    mPlayer.setDataSource(result.getVideoUri().toString());
                    mPlayer.setOnPreparedListener(PlayerService.this);
                    mPlayer.setOnCompletionListener(PlayerService.this);
                    mPlayer.setOnErrorListener(PlayerService.this);
                    mPlayer.prepareAsync();

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onFailure(Error error) {
            Utils.toLog(error.toString());
            Utils.toLog(error.getMessage());
        }
    };
}