package com.kkx.player;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.kkx.player.PlayerService.Action;
import com.kkx.player.Utils.LinkType;

/**
 * @author Maxim Ambroskin kkxmshu@gmail.com
 * @since 22/12/16
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle b) {
        super.onCreate(b);


        Intent intent = getIntent();
        if (intent != null) {
            String url = intent.getDataString();
            if (url != null && url.contains(LinkType.NORMAL)) {
                intent = new Intent(Action.YOUTUBE).putExtra(PlayerService.EXTRA_LINK, url)
                        .setPackage(getPackageName());
                startService(intent);

            } else if (intent.hasExtra(Intent.EXTRA_TEXT)) {
                url = intent.getStringExtra(Intent.EXTRA_TEXT);
                Utils.toLog(url);
                intent = new Intent(Action.YOUTUBE).putExtra(PlayerService.EXTRA_LINK, url)
                        .setPackage(getPackageName());
                startService(intent);
            }
            finish();
        }
    }
}