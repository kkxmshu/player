package com.kkx.player;

import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * @author Maxim Ambroskin kkxmshu@gmail.com
 * @since 22/12/16
 */
@SuppressWarnings("WeakerAccess")
public class Utils {

    public interface LinkType {
        String SHORT = "youtu.be";
        String NORMAL = "youtube.com";
    }

    public static String parse(Uri uri) {
        final String path = uri.getAuthority();
        if (path.equals(LinkType.SHORT)) {
            return uri.getLastPathSegment();
        }
        return uri.getQueryParameter("v");
    }

    public static String parse(String string) {
        return parse(Uri.parse(string));
    }

    public static void toLog(@Nullable String message) {
        Log.d(PlayerService.class.getSimpleName(), ">>> " + message);
    }
}